# OutlookQuickPrint
OutlookQuickPrint is a simple outlook addin that prints the first page of a selected email using a shortcut

After running the setup open your Outlook application select an email and press alt+P

Depending on the speed of your computer, you might see this small window appear at the top of your screen for a second and then disappear.


![image](https://user-images.githubusercontent.com/94911727/156022213-ea862138-ebcd-490d-94c0-348d6c313637.png)

The AddIn quick-prints to your default printer without any further confirmation
